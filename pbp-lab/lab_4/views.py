
from django.shortcuts import render
from .models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)

@login_required
def add_note(request):
    ##user = request.user 
    context = {}
    form = NoteForm(request.POST or None)
    if form.is_valid():
        # save the form data to model
        form.save()

    context['form']= form
    return render(request, "lab4_form.html", context)
