
from django.shortcuts import render
from .models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required, user_passes_test

# Create your views here.
@login_required
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required
def add_friend(request):
    ##user = request.user 
    context = {}
    form = FriendForm(request.POST or None)
    if form.is_valid():
        # save the form data to model
        form.save()

    context['form']= form
    return render(request, "lab3_form.html", context)