1.Apakah perbedaan antara JSON dan XML?
Json dan XML sama - sama merupakan format untuk pertukaran data antara berbagai sistem di web

Perbedaan   :
JSON
1.JSON (Notasi Objek JavaScript) standar berbasis teks dalam pengolahan data
2.Tipe bahasa meta
3.Kompleksitasnya sederhana dan mudah dibaca
4.Berorientasi pada data
5.Mendukung array
6.Ekstensi file diakhiri .json

XML
1.XML (bahasa markup extensible) format independen perangkat lunak dan keras dalam pertukaran data
2.Tipe bahasa markup
3.Lebih rumit
4.Berorientasi pada dokumen
5.Tidak mendukung array
6.Ekstensi file diakhiri .xml

2.Apakah perbedaan antara HTML dan XML?
HTML (HyperText Markup Language) adalah bahasa pemrograman yang digunakan untuk membuat kerangka website

XML (eXtensible Markup Language) juga digunakan untuk membuat halaman web dan aplikasi web. Tetapi ini adalah bahasa dinamis yang digunakan untuk mengangkut data dan bukan untuk menampilkan data. Tujuan desain XML berfokus pada kesederhanaan, umum, dan kegunaan di Internet.

Perbedaan   :
1.XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
2.XML didorong konten sedangkan HTML didorong oleh format.
3.XML itu Case Sensitive sedangkan XML Case Insensitive
4.XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
5.XML strict untuk tag penutup sedangkan HTML tidak strict.
6.Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
7.Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.

Referensi:
https://blogs.masterweb.com/perbedaan-xml-dan-html/
https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html

