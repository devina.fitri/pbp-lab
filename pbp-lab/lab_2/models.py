from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=150)
    from1 = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    message = models.CharField(max_length=150)
    
##json_string = json.dumps(Note, indent=4)


