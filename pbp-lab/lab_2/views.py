from django.shortcuts import render
from django.http.response import HttpResponse
from django.views.generic import ListView
from .models import Note
from django.core import serializers
from django.http import JsonResponse
import json

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(data):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(data):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")